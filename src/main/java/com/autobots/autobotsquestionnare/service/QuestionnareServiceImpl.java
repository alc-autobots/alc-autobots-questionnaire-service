package com.autobots.autobotsquestionnare.service;

import com.autobots.autobotsquestionnare.entity.Questionnare;
import com.autobots.autobotsquestionnare.repository.QuestionnareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Profile(value = { "local", "dev" })
public class QuestionnareServiceImpl implements QuestionnareService {
    @Autowired
    private final QuestionnareRepository questionnareRepository;

    public QuestionnareServiceImpl(QuestionnareRepository questionnareRepository) {
        this.questionnareRepository = questionnareRepository;
    }

    @Override
    public List<Questionnare> getQuestionnareList() {
        return questionnareRepository.findAll();
    }

    @Override
    public Questionnare getQuestionnareById(String id) {
        Optional<Questionnare> existindData = questionnareRepository.findById(id);
        Questionnare questionnare = null;
        if(existindData.isPresent()){
            questionnare = existindData.get();
        }
        return questionnare;
    }

    @Override
    public Questionnare addNewQuestionnare(Questionnare questionnare) {
        Questionnare newQuestionnare = null;
        Optional<Questionnare> existId = questionnareRepository.findById(questionnare.getId());
        if(existId.isPresent()){
            newQuestionnare= existId.get();
            newQuestionnare.setId(questionnare.getId());
            newQuestionnare.setWebCtrlVersion(questionnare.getWebCtrlVersion());
            newQuestionnare.setWebCtrlIp(questionnare.getWebCtrlIp());
            newQuestionnare.setAddOnsList(questionnare.getAddOnsList());
            newQuestionnare.setOldAddOns(questionnare.getOldAddOns());
            newQuestionnare.setAlertMethod(questionnare.getAlertMethod());
            newQuestionnare.setInNotification(questionnare.getInNotification());
            newQuestionnare.setDbBrandAndVersion(questionnare.getDbBrandAndVersion());
            newQuestionnare.setUsingHierarchial(questionnare.getUsingHierarchial());
            newQuestionnare.setDirectorySize(questionnare.getDirectorySize());
            newQuestionnare.setDbSize(questionnare.getDbSize());
            newQuestionnare.setControllerNo(questionnare.getControllerNo());
            newQuestionnare.setPointsNo(questionnare.getPointsNo());
            newQuestionnare.setSourceNo(questionnare.getSourceNo());
            newQuestionnare.setSampleNo(questionnare.getSampleNo());
            newQuestionnare.setControllerModel(questionnare.getControllerModel());
            newQuestionnare.setControllerIpAddress(questionnare.getControllerIpAddress());
            newQuestionnare.setControllerFirmware(questionnare.getControllerFirmware());
            newQuestionnare.setBacNet(questionnare.getBacNet());
            newQuestionnare.setInternetProxy(questionnare.getInternetProxy());
            newQuestionnare.setHardwareVpn(questionnare.getHardwareVpn());
            questionnareRepository.save(newQuestionnare);
        }else {
            newQuestionnare = new Questionnare();
            newQuestionnare.setId(questionnare.getId());
            newQuestionnare.setWebCtrlVersion(questionnare.getWebCtrlVersion());
            newQuestionnare.setWebCtrlIp(questionnare.getWebCtrlIp());
            newQuestionnare.setAddOnsList(questionnare.getAddOnsList());
            newQuestionnare.setOldAddOns(questionnare.getOldAddOns());
            newQuestionnare.setAlertMethod(questionnare.getAlertMethod());
            newQuestionnare.setInNotification(questionnare.getInNotification());
            newQuestionnare.setDbBrandAndVersion(questionnare.getDbBrandAndVersion());
            newQuestionnare.setUsingHierarchial(questionnare.getUsingHierarchial());
            newQuestionnare.setDirectorySize(questionnare.getDirectorySize());
            newQuestionnare.setDbSize(questionnare.getDbSize());
            newQuestionnare.setControllerNo(questionnare.getControllerNo());
            newQuestionnare.setPointsNo(questionnare.getPointsNo());
            newQuestionnare.setSourceNo(questionnare.getSourceNo());
            newQuestionnare.setSampleNo(questionnare.getSampleNo());
            newQuestionnare.setControllerModel(questionnare.getControllerModel());
            newQuestionnare.setControllerIpAddress(questionnare.getControllerIpAddress());
            newQuestionnare.setControllerFirmware(questionnare.getControllerFirmware());
            newQuestionnare.setBacNet(questionnare.getBacNet());
            newQuestionnare.setInternetProxy(questionnare.getInternetProxy());
            newQuestionnare.setHardwareVpn(questionnare.getHardwareVpn());
            questionnareRepository.save(newQuestionnare);
        }
        return newQuestionnare;
    }

    @Override
    public Questionnare editExistingQuestionnare(Questionnare questionnare, String id) {
        Optional<Questionnare> existindData = questionnareRepository.findById(id);
        Questionnare newQuestionnare = null;
        if(existindData.isPresent()){
            newQuestionnare= existindData.get();
            newQuestionnare.setId(questionnare.getId());
            newQuestionnare.setWebCtrlVersion(questionnare.getWebCtrlVersion());
            newQuestionnare.setWebCtrlIp(questionnare.getWebCtrlIp());
            newQuestionnare.setAddOnsList(questionnare.getAddOnsList());
            newQuestionnare.setOldAddOns(questionnare.getOldAddOns());
            newQuestionnare.setAlertMethod(questionnare.getAlertMethod());
            newQuestionnare.setInNotification(questionnare.getInNotification());
            newQuestionnare.setDbBrandAndVersion(questionnare.getDbBrandAndVersion());
            newQuestionnare.setUsingHierarchial(questionnare.getUsingHierarchial());
            newQuestionnare.setDirectorySize(questionnare.getDirectorySize());
            newQuestionnare.setDbSize(questionnare.getDbSize());
            newQuestionnare.setControllerNo(questionnare.getControllerNo());
            newQuestionnare.setPointsNo(questionnare.getPointsNo());
            newQuestionnare.setSourceNo(questionnare.getSourceNo());
            newQuestionnare.setSampleNo(questionnare.getSampleNo());
            newQuestionnare.setControllerModel(questionnare.getControllerModel());
            newQuestionnare.setControllerIpAddress(questionnare.getControllerIpAddress());
            newQuestionnare.setControllerFirmware(questionnare.getControllerFirmware());
            newQuestionnare.setBacNet(questionnare.getBacNet());
            newQuestionnare.setInternetProxy(questionnare.getInternetProxy());
            newQuestionnare.setHardwareVpn(questionnare.getHardwareVpn());
            questionnareRepository.save(newQuestionnare);
        }
        return newQuestionnare;
    }

    @Override
    public void deleteQuestionnareByid(String id) {
        questionnareRepository.deleteById(id);
    }
}
