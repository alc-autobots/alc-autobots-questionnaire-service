##https://medium.com/swlh/build-a-docker-image-using-maven-and-spring-boot-58147045a400
FROM openjdk:8
ARG JAR_FILE=/target/*.jar
#ADD ${JAR_FILE} alc-autobots-questionnare.jar
VOLUME /tmp
EXPOSE 7071
COPY ${JAR_FILE} questionnaire.jar
ENTRYPOINT ["java", "-jar", "/questionnaire.jar"]